"use strict";

class JsTest {
  constructor(name, description, boilerplate) {
    this.name = name
    this.description = description
    this.boilerplate = boilerplate
    this.cases = []
  }

  case(name, fn) {
    this.cases.push({
      test: fn,
      name: name
    })
    return this
  }

  async evalCase(x, test) {
    const mark = Date.now()
    try {
      const res = await new Promise((y, n) => {
        test.test(x, y, n)
      })
      return {
        res: true,
        out: res || "Ok",
        name: test.name,
        time: Date.now() - mark,
      }
    } catch (e) {
      return {
        res: false,
        out: e.message || e || "Unknown error",
        name: test.name,
        time: Date.now() - mark,
      }
    }
  }

  async eval(code) {
    const wrap = `
      "use strict";
      const module = {}
      ${code}
      return module
    `
    const x = Function(wrap)()
    return await Promise.all(this.cases.map(test => this.evalCase(x, test)))
  }
}