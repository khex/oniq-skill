const TimeTest = new JsTest("Time",
`
Write a function that measures the execution time of a function.`,
`
/**
 * Return the execution time of the supplied function.
 *
 * @param  {Function} fn The function to execute.
 * @param  {...*} args Arguments to call fn with
 * @return {Promise<number>} Future of execution time.
 */
module.time = function (fn, ...args) {
    return 0
}

// e.g. let ms = await time(curl, 'http://google.com')
`)
.case("fn(100)", async (x, y, n) => {
  const sleep = ms => new Promise((F, R) => setTimeout(F, ms))
  const fn = async (a) => await sleep(a)
  const ms = await x.time(fn, 100)
  const dt = Math.abs(ms - 100)
  dt < 20 ? y() : n(`Got ${ms} ms, wanted ${100} ms`)
})
.case("fn(100, 200, 200)", async (x, y, n) => {
  const sleep = ms => new Promise((F, R) => setTimeout(F, ms))
  const fn = async (a, b, c) => await sleep(a+b+c)
  const ms = await x.time(fn, 100, 200, 200)
  const dt = Math.abs(ms - 500)
  dt < 20 ? y() : n(`Got ${ms} ms, wanted ${500} ms`)
})

