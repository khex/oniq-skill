const SleepTest = new JsTest("Sleep",
`
Write a sleep function.`,
`
/**
 * "Sleeps" for the specified amount of milliseconds.
 *
 * @param  {number} millisec Milliseconds to wait before
 *                           resolving the promise.
 * @return {Promise}
 */
module.sleep = function (millisec) {
    return Promise.resolve()
}

// e.g. await sleep(1000)
// e.g. sleep(1000).then(() => console.log('yes'))
`)
.case("1 sec", async (x, y, n) => {
  const ts = Date.now()
  await x.sleep(1000)
  const dt = Date.now() - ts
  const t = Math.abs(dt - 1000)
  t < 100 ? y() : n(`Waited for ${dt} ms, wanted ${1000}`)
})
.case("500 ms", async (x, y, n) => {
  const ts = Date.now()
  await x.sleep(500)
  const dt = Date.now() - ts
  const t = Math.abs(dt - 500)
  t < 100 ? y() : n(`Waited for ${dt} ms, wanted ${500}`)
})

