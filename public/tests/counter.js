const CounterTest = new JsTest("Counter",
`
Implement the count function, which computes the frequency of each value in an array.`,
`
/**
 * Compute the frequency of values in an array.
 *
 * @param  {array} data Array of values (strings, numbers)
 * @return {object}     A mapping of value -> count
 */
module.count = function (data) {
    return {}
}
`)
.case("simple", async (x, y, n) => {
  const i = ["a", "b", "a", "b", "c"]
  const r = { a: 2, b: 2, c: 1 }
  const o = await x.count(i)
  const q = Object.keys(r).filter(k => r[k]!==o[k]).length === 0
  q ? y() : n(JSON.stringify(o, null, 4))
})
.case("complex", async (x, y, n) => {
  const i = ["a", "w", "b", "a", "b", "c", 5, 4, 5]
  const r = { a: 2, b: 2, c: 1, w: 1, 4: 1, 5: 2 }
  const o = await x.count(i)
  const q = Object.keys(r).filter(k => r[k]!==o[k]).length === 0
  q ? y() : n(JSON.stringify(o, null, 4))
})

