const ParserTest = new JsTest("Parser",
`
Write a function which parses a given string and returns the application number.
`,
`
/**
 * Try to find an application number in a string.
 * Application numbers are on the form:
 *
 *   2000-y-99
 *   1967-z-00
 *
 * @param  {string} str The input string
 * @return {string}     The first number if found,
                        or "No number" if not found.
 */
module.parse = function (str) {
    return "No number"
}
`)
.case("simple", (x, y, n) => {
  const ref = "2005-x-66"
  const res = x.parse("2005-x-66")
  res === ref ? y(ref) : n(`Got '${res}', wanted '${ref}'`)
})
.case("in a substring", (x, y, n) => {
  const ref = "1993-z-15"
  const res = x.parse("Application Number (1993-z-15)")
  res === ref ? y(ref) : n(`Got '${res}', wanted '${ref}'`)
})
.case("no number", (x, y, n) => {
  const ref = "No number"
  const res = x.parse("wtf no nomber")
  res === ref ? y(ref) : n(`Got '${res}', wanted '${ref}'`)
})
