window.addEventListener('load', evt => {
    // pick test gui
    const testcase = document.querySelector('select[name="testcase"]')

    // run check() on editor change event?
    const autorun  = document.querySelector('input[name="autorun"]')

    // ui text
    const title    = document.querySelector('#test')
    const desc     = document.querySelector('#desc')

    document.querySelector('button[name="exec"]')
        .addEventListener('click', evt => check())

    document.querySelector('button[name="next"]')
        .addEventListener('click', evt => next())

    const editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");

    const output = ace.edit('output')
    output.setTheme("ace/theme/monokai");
    output.setReadOnly(true)
    output.renderer.setShowGutter(false)

    const tests = [
        ParserTest,
        SleepTest,
        CounterTest,
        TimeTest,
    ]

    // use one session per test to keep text safe when switching between tests
    const history = tests
        .map(test => new ace.EditSession(test.boilerplate))

    history.forEach(session => {
        session.setMode("ace/mode/javascript")
        session.on('change', autorunCheck)
    })

    var activeTest = -1

    async function init(idx) {
        activeTest = idx
        testcase.value = idx

        editor.setSession(history[activeTest])

        title.textContent = `Test ${idx+1} - ${tests[activeTest].name}`
        desc.textContent = tests[activeTest].description

        await check()
    }

    async function check() {
        if (!tests[activeTest]) return

        var text = "?"
        output.session.setValue("running...")
        try {
            const mark = Date.now()
            const res = await tests[activeTest].eval(editor.getValue())
            const pass = res.every(tr => tr.res)

            const list = res.map(tr => {
                const pass = tr.res ? "✓" : "✗"
                return `[${pass}] ${tr.name}\n${tr.out}`
            }).join('\n\n')

            const name = tests[activeTest].name
            const time = new Date().toLocaleTimeString()
            text = `${name}, ${time}

${list}

${res.length} tests evaluated in ${Date.now() - mark} ms.
${pass ? "All test pass." : "Errors found, please try again."}`
        } catch (e) {
            text = e.message
        }

        output.session.setValue(text)
    }

    function next() {
        init((activeTest + 1) % tests.length)
    }

    function autorunCheck (delta) {
        if (!autorun.checked) return
        check()
    }

    testcase.addEventListener('change', evt => {
        init(parseInt(testcase.value))
    })

    tests.forEach((test, idx) => {
        const opt = document.createElement('option')
        opt.value = idx
        opt.textContent = `${idx+1} - ${test.name}`
        testcase.appendChild(opt)
    })

    init(0)
})
